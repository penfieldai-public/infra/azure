

## To Create Open AI Instance
### Create Resource Group

Create the Resource Group with the required name and location accordingly. e.g:
```
az group create \
  --name "RESOURCE_GROUP_NAME" \
  --location "REGION"
```

### Deploy Template

create the Deployment using command below, make sure to update resource group name and account name accordingly. `accountName` is the name of the OpenAI instance.
```
az deployment group create \
  --name "Penfield-App-OpenAI" \
  --resource-group "RESOURCE_GROUP_NAME" \
  --template-file mainTemplate.json \
  --parameters accountName=NAME_OF_OPEN_AI_INSTANCE
```

## Retrieve Outputs:
To retrieve the outputs of the deployment, you can use the following command:
```
az deployment group show \
  --name "Penfield-App-OpenAI" \
    --resource-group "RESOURCE_GROUP_NAME" \
  --query "properties.outputs"
```

## Remove Deployment and its resources
If you have to delete the deployment and its resources, you can use the following command, be careful about this command because this will delete all the other resources to be delete in that resource group

```
az group delete \
  --name "RESOURCE_GROUP_NAME" \
  --yes
```

You may have to purge the resources if you plan to recreate using same name:

 ```
az cognitiveservices account purge --name "NAME_OF_OPEN_AI_INSTANCE" --resource-group "RESOURCE_GROUP_NAME" --subscription "SUBSCRIPTION_ID" --location "LOCATION"
 ```