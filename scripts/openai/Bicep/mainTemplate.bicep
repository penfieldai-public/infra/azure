param accountName string 
param location string = resourceGroup().location 

param skuName string = 'S0'

param models array = [
  {
    deploymentName: 'text-embedding-ada-002'
    modelName: 'text-embedding-ada-002'
    version: '2'
    capacity: 120
  }
  {
    deploymentName: 'gpt-35-turbo'
    modelName: 'gpt-35-turbo'
    version: '0125'
    capacity: 120
  }
]

resource cognitiveServiceAccount 'Microsoft.CognitiveServices/accounts@2023-05-01' = {
  name: accountName
  location: location
  kind: 'OpenAI'
  sku: {
    name: skuName
  }
  properties: {}
}

resource deployments 'Microsoft.CognitiveServices/accounts/deployments@2023-05-01' = [for (model, idx) in models: {
  parent: cognitiveServiceAccount
  name: '${model.deploymentName}'
  properties: {
    model: {
      name: model.modelName
      version: model.version
      format: 'OpenAI'
    }
    versionUpgradeOption: 'OnceNewDefaultVersionAvailable'
  }
  sku: {
    name: 'Standard'
    capacity: model.capacity
  }
}]

output Azure_OPENAI_Endpoint string = cognitiveServiceAccount.properties.endpoint
output Azure_OPENAI_API_Key string = cognitiveServiceAccount.listKeys().key1
