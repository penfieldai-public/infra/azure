param (
    [string]$subscriptionId
)

# Check if a subscription ID is provided
if (-not $subscriptionId) {
    Write-Output "Usage: .\Register-And-Verify-Providers.ps1 -subscriptionId <subscription-id>"
    exit 1
}

# Set the subscription context
az account set --subscription $subscriptionId

# Display the current subscription
$currentSubscription = az account show --query "name" -o tsv
Write-Output "Running script for subscription: $currentSubscription"

# Array of resource providers to register
$providers = @(
    "Microsoft.Network",
    "Microsoft.Compute",
    "Microsoft.Storage",
    "microsoft.insights",
    "Microsoft.OperationalInsights",
    "Microsoft.KubernetesConfiguration"
)

# Function to check if a provider is registered
function Is-Registered {
    param (
        [string]$provider
    )
    $status = az provider show --namespace $provider --query "registrationState" -o tsv
    return $status -eq "Registered"
}

# Register and verify each provider
foreach ($provider in $providers) {
    if (Is-Registered -provider $provider) {
        Write-Output "$provider is already registered."
    } else {
        Write-Output "Registering $provider..."
        az provider register --namespace $provider
        
        Write-Output "Verifying $provider registration..."
        while (-not (Is-Registered -provider $provider)) {
            Write-Output "$provider is not registered yet. Waiting..."
            Start-Sleep -Seconds 10
        }

        Write-Output "$provider has been registered."
    }
}

Write-Output "All specified resource providers have been processed."
