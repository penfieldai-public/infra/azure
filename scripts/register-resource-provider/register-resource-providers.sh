#!/bin/bash

# Check if a subscription ID is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <subscription-id>"
  exit 1
fi

subscription_id=$1

# Set the subscription context
az account set --subscription "$subscription_id"

# Display the current subscription
current_subscription=$(az account show --query "name" -o tsv)
echo "Running script for subscription: $current_subscription"

# Array of resource providers to register
providers=(
  "Microsoft.Network"
  "Microsoft.Compute"
  "Microsoft.Storage"
  "microsoft.insights"
  "Microsoft.OperationalInsights"
  "Microsoft.KubernetesConfiguration"
)

# Function to check if a provider is registered
is_registered() {
  local provider=$1
  az provider show --namespace "$provider" --query "registrationState" -o tsv | grep -q "Registered"
}

# Register and verify each provider
for provider in "${providers[@]}"; do
  if is_registered "$provider"; then
    echo "$provider is already registered."
  else
    echo "Registering $provider..."
    az provider register --namespace "$provider"
    
    echo "Verifying $provider registration..."
    while ! is_registered "$provider"; do
      echo "$provider is not registered yet. Waiting..."
      sleep 10
    done

    echo "$provider has been registered."
  fi
done

echo "All specified resource providers have been processed."
