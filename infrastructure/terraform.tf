# Required configuration of Azure Provider
terraform {

  # Required version for terraform CLI
  required_version = "~> 1.0"

  # Required version of Terrform provider registry
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.58"
    }
  }
}
