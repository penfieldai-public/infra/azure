output "virtual_network_id" {
  value = module.vnet.vnet_id
}

output "penfieldai_rg_name" {
  value = var.penfieldai_rg_name
}

output "aks_cluster_name" {
  value = var.cluster_name
}

output "azure_subscription_id" {
  value = var.azure_subscription_id
}