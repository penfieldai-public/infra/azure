resource "random_id" "kvname" {
  count = var.create_db ? 1 : 0

  byte_length = 5
  prefix      = "keyvault"
}

data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "kv1" {
  count = var.create_db ? 1 : 0

  depends_on                  = [azurerm_resource_group.penfieldai_resources, module.vnet]
  name                        = random_id.kvname[0].hex
  location                    = azurerm_resource_group.penfieldai_resources.location
  resource_group_name         = var.penfieldai_rg_name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false
  sku_name                    = "standard"
  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id
    key_permissions = [
      "Get",
    ]
    secret_permissions = [
      "Get", "Backup", "Delete", "List", "Purge", "Recover", "Restore", "Set",
    ]
    storage_permissions = [
      "Get",
    ]
  }
}

resource "random_password" "dbpassword" {
  count = var.create_db ? 1 : 0

  length           = 32
  special          = true
  override_special = "-_%@!#+:*&()"
}

resource "azurerm_key_vault_secret" "dbpassword" {
  count = var.create_db ? 1 : 0

  name         = "dbpassword"
  value        = random_password.dbpassword[0].result
  key_vault_id = azurerm_key_vault.kv1[0].id
  depends_on   = [azurerm_key_vault.kv1, module.vnet]
}

# resource "azurerm_network_security_group" "default" { ## Can be used to setup NSGs
#   name                = "${var.name_prefix}-nsg"
#   location            = azurerm_resource_group.penfieldai_resources.location
#   resource_group_name = azurerm_resource_group.penfieldai_resources.name
#
#   security_rule {
#     name                       = "AllowEverything"
#     priority                   = 100
#     direction                  = "Inbound"
#     access                     = "Allow"
#     protocol                   = "Tcp"
#     source_port_range          = "*"
#     destination_port_range     = "*"
#     source_address_prefix      = "*"
#     destination_address_prefix = "*"
#   }
#   depends_on = [module.vnet]
# }

# resource "azurerm_subnet_network_security_group_association" "default" { ##Attaches NSG to the subnet
#   subnet_id                 = azurerm_subnet.default.id
#   network_security_group_id = azurerm_network_security_group.default.id
#   depends_on = [module.vnet]
# }

resource "azurerm_private_dns_zone" "default" {
  count = var.create_db ? 1 : 0

  name                = "${var.penfieldai_rg_name}-pdz.postgres.database.azure.com"
  resource_group_name = azurerm_resource_group.penfieldai_resources.name
  depends_on          = [module.vnet]
}
#
resource "azurerm_private_dns_zone_virtual_network_link" "link_to_private_dns_zone" {
  count = var.create_db ? 1 : 0

  name                  = "virtual-network-link"
  private_dns_zone_name = azurerm_private_dns_zone.default[0].name
  virtual_network_id    = module.vnet.vnet_id
  resource_group_name   = azurerm_resource_group.penfieldai_resources.name
  depends_on            = [module.vnet, azurerm_private_dns_zone.default]
}

resource "azurerm_postgresql_flexible_server" "default" {
  count = var.create_db ? 1 : 0

  name                   = "${var.database_server_name}-server"
  resource_group_name    = azurerm_resource_group.penfieldai_resources.name
  location               = azurerm_resource_group.penfieldai_resources.location
  version                = var.database_version
  delegated_subnet_id    = module.vnet.vnet_subnets[2]
  private_dns_zone_id    = azurerm_private_dns_zone.default[0].id
  administrator_login    = var.database_username
  administrator_password = azurerm_key_vault_secret.dbpassword[0].value
  storage_mb             = 2097152
  sku_name               = var.database_server_size
  backup_retention_days  = var.retention
  high_availability {
    mode                      = "ZoneRedundant"
    standby_availability_zone = "2"
  }
  zone = "1"
  tags = {
    Environment = var.environment
    Terraform   = var.terraform
    Project     = var.project
  }

  depends_on = [module.vnet, azurerm_private_dns_zone_virtual_network_link.link_to_private_dns_zone, azurerm_key_vault_secret.dbpassword]
}
