data "azuread_group" "aks_cluster_admins" {
  display_name = "penfield_aks_cluster_admins"
}

module "aks" {
  source  = "Azure/aks/azurerm"
  version = "7.1.0"

  resource_group_name                     = azurerm_resource_group.penfieldai_resources.name
  client_id                               = var.client_id
  client_secret                           = var.client_secret
  kubernetes_version                      = var.kubernetes_version
  orchestrator_version                    = var.kubernetes_version
  prefix                                  = "${var.cluster_name}-dns"
  cluster_name                            = var.cluster_name
  network_plugin                          = var.network_plugin
  vnet_subnet_id                          = module.vnet.vnet_subnets[1]
  os_disk_size_gb                         = var.os_disk_size_gb
  sku_tier                                = var.sku_tier
  role_based_access_control_enabled       = var.role_based_access_control_enabled
  rbac_aad                                = var.rbac_aad
  rbac_aad_managed                        = var.rbac_aad_managed
  rbac_aad_admin_group_object_ids         = [data.azuread_group.aks_cluster_admins.id]
  public_network_access_enabled           = var.public_network_access_enabled
  api_server_authorized_ip_ranges         = var.api_server_authorized_ip_ranges
  private_cluster_enabled                 = var.private_cluster_enabled
  http_application_routing_enabled        = var.http_application_routing_enabled
  azure_policy_enabled                    = var.azure_policy_enabled
  enable_auto_scaling                     = var.enable_auto_scaling
  enable_host_encryption                  = var.enable_host_encryption
  agents_min_count                        = var.agents_min_count
  agents_max_count                        = var.agents_max_count
  agents_count                            = var.agents_count
  agents_max_pods                         = var.agents_max_pods
  agents_pool_name                        = var.agents_pool_name
  temporary_name_for_rotation             = var.temporary_name_for_rotation
  agents_availability_zones               = var.agents_availability_zones
  agents_type                             = var.agents_type
  agents_size                             = var.agents_size
  ingress_application_gateway_enabled     = var.ingress_application_gateway_enabled
  ingress_application_gateway_name        = var.ingress_application_gateway_name
  ingress_application_gateway_subnet_cidr = var.ingress_application_gateway_subnet_cidr

  network_policy             = var.network_policy
  net_profile_dns_service_ip = var.net_profile_dns_service_ip
  net_profile_service_cidr   = var.net_profile_service_cidr

  depends_on = [module.vnet]
}
