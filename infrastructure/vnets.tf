module "vnet" {
  source  = "Azure/vnet/azurerm"
  version = "4.1.0"

  vnet_name           = var.vnet_name_penfieldai
  resource_group_name = azurerm_resource_group.penfieldai_resources.name
  use_for_each        = var.use_for_each
  address_space       = ["${var.address_space}"]
  subnet_prefixes     = ["${var.public_subnet_prefix}", "${var.web_subnet_prefix}", "${var.db_subnet_prefix}"]
  subnet_names        = ["${var.public_subnet1_name}", "${var.web_subnet1_name}", "${var.db_subnet1_name}"]
  vnet_location       = var.penfieldai_rg_location
  subnet_service_endpoints = {
    "${var.db_subnet1_name}" : ["Microsoft.Sql", "Microsoft.Storage"]
  }
  subnet_delegation = { ##Required for flexibleServers
    db-subnet-1 = {
      "Microsoft.DBforPostgreSQL/flexibleServers" = {
        service_name = "Microsoft.DBforPostgreSQL/flexibleServers"
        service_actions = [
          "Microsoft.Network/virtualNetworks/subnets/join/action",
        ]
      }
    }
  }

  tags = {
    Environment = var.environment
    Terraform   = var.terraform
    Project     = var.project
  }
  depends_on = [azurerm_resource_group.penfieldai_resources]
}
## Variables done
