resource "azurerm_resource_group" "penfieldai_resources" {
  location = var.penfieldai_rg_location
  name     = var.penfieldai_rg_name
}
