########################
## General Variables ##
########################

variable "environment" {
  description = "Name of the environment"
  type        = string
  default     = "prod"
}

variable "project" {
  description = "Name of the project"
  type        = string
  default     = "penfieldai"
}

variable "terraform" {
  description = "Managed by Terraform"
  type        = string
  default     = "true"
}

variable "azure_subscription_id" {
  description = "Subscription ID of Azure"
  type        = string
  default     = "" # your-azure-subscription-id
}

variable "penfieldai_rg_name" {
  type    = string
  default = "penfieldai-resources-rg"
}

variable "penfieldai_rg_location" {
  type    = string
  default = "canadacentral"
}
##########################
## Networking Variables ##
##########################

variable "web_subnet1_name" {
  type    = string
  default = "web-subnet-1"
}

variable "public_subnet1_name" {
  type    = string
  default = "public-subnet-1"
}

variable "db_subnet1_name" {
  type    = string
  default = "db-subnet-1"
}

variable "vnet_name_penfieldai" {
  type    = string
  default = "penfieldai-vnet"
}

variable "use_for_each" {
  type    = bool
  default = true
}

variable "address_space" {
  type    = string
  default = "10.250.0.0/16"
}

variable "public_subnet_prefix" {
  type    = string
  default = "10.250.0.0/20"
}

variable "web_subnet_prefix" {
  type    = string
  default = "10.250.32.0/20"
}

variable "db_subnet_prefix" {
  type    = string
  default = "10.250.64.0/20"
}

########################
## Database Variables ##
########################

variable "create_db" {
  description = "Controls if DB server should be created"
  type        = bool
  default     = false
}

variable "database_server_name" {
  type    = string
  default = "penfieldai-postgresql"
}

variable "database_version" {
  type    = string
  default = "13"
}

variable "database_username" {
  type    = string
  default = "penfieldaiadm" #This is just an example
}

variable "db_name1" {
  type    = string
  default = "penfieldai"
}

variable "db_name2" {
  type    = string
  default = "penfieldai-app"
}

variable "database_server_size" {
  type    = string
  default = "GP_Standard_D2s_v3"
}

variable "retention" {
  type    = number
  default = 30
}

###################
## AKS Variables ##
###################


variable "client_id" {
  type        = string
  default     = ""
  description = "(Optional) The Client ID (appId) for the Service Principal used for the AKS deployment"
  nullable    = false
}

variable "client_secret" {
  type        = string
  default     = ""
  description = "(Optional) The Client Secret (password) for the Service Principal used for the AKS deployment"
  nullable    = false
}

variable "kubernetes_version" {
  type        = string
  default     = "1.29"
  description = "Specify which Kubernetes release to use. The default used is the latest Kubernetes version available in the region"
}

variable "cluster_name" {
  type        = string
  default     = "penfieldai-aks-cluster"
  description = "(Optional) The name for the AKS resources created in the specified Azure Resource Group. This variable overwrites the 'prefix' var (The 'prefix' var will still be applied to the dns_prefix if it is set)"
}

variable "network_plugin" {
  type        = string
  default     = "azure"
  description = "Network plugin to use for networking."
  nullable    = false
}

variable "os_disk_size_gb" {
  type        = string
  default     = "50"
  description = "Disk size of nodes in GBs."
}

variable "sku_tier" {
  type        = string
  default     = "Standard" # defaults to Free
  description = "The SKU Tier that should be used for this Kubernetes Cluster. Possible values are `Free` and `Standard`"

  validation {
    condition     = contains(["Free", "Standard"], var.sku_tier)
    error_message = "The SKU Tier must be either `Free` or `Standard`. `Paid` is no longer supported since AzureRM provider v3.51.0."
  }
}

variable "role_based_access_control_enabled" {
  type        = bool
  default     = true
  description = "Enable Role Based Access Control."
  nullable    = false
}

variable "rbac_aad" {
  type        = bool
  default     = true
  description = "(Optional) Is Azure Active Directory integration enabled?"
  nullable    = false
}

variable "rbac_aad_managed" {
  type        = bool
  default     = true
  description = "Is the Azure Active Directory integration Managed, meaning that Azure will create/manage the Service Principal used for integration."
  nullable    = false
}

variable "rbac_aad_admin_group_object_ids" {
  type        = list(string)
  default     = ["penfield_aks_cluster_admins"]
  description = "Object ID of groups with admin access."
}

variable "public_network_access_enabled" {
  type        = bool
  default     = true
  description = "(Optional) Whether public network access is allowed for this Kubernetes Cluster. Defaults to `true`. Changing this forces a new resource to be created."
  nullable    = false
}

variable "api_server_authorized_ip_ranges" {
  type        = set(string)
  default     = ["163.116.141.0/24", "163.116.130.0/24", "163.116.142.0/24"] # These are default penfield ZTNA IP ranges.
  description = "(Optional) The IP ranges to allow for incoming traffic to the server nodes."
}

variable "private_cluster_enabled" {
  type        = bool
  default     = false
  description = "If true cluster API server will be exposed only on internal IP address and available only in cluster vnet."
}

variable "http_application_routing_enabled" {
  type        = bool
  default     = false
  description = "Enable HTTP Application Routing Addon (forces recreation)."
}

variable "azure_policy_enabled" {
  type        = bool
  default     = false
  description = "Enable Azure Policy Addon."
}

variable "enable_auto_scaling" {
  type        = bool
  default     = true
  description = "Enable node pool autoscaling"
}

variable "enable_host_encryption" {
  type        = bool
  default     = true
  description = "Enable Host Encryption for default node pool. Encryption at host feature must be enabled on the subscription: https://docs.microsoft.com/azure/virtual-machines/linux/disks-enable-host-based-encryption-cli"
}

variable "agents_min_count" {
  type        = number
  default     = 2
  description = "Minimum number of nodes in a pool"
}

variable "agents_max_count" {
  type        = number
  default     = 5
  description = "Maximum number of nodes in a pool"
}

variable "agents_count" {
  type        = number
  default     = null
  description = "The number of Agents that should exist in the Agent Pool. Please set `agents_count` `null` while `enable_auto_scaling` is `true` to avoid possible `agents_count` changes."
}

variable "agents_max_pods" {
  type        = number
  default     = 100
  description = "(Optional) The maximum number of pods that can run on each agent. Changing this forces a new resource to be created."
}

variable "agents_pool_name" {
  type        = string
  default     = "webnodepool"
  description = "The default Azure AKS agentpool (nodepool) name."
  nullable    = false
}

variable "temporary_name_for_rotation" {
  type        = string
  default     = "webnodepool1"
  description = "(Optional) Specifies the name of the temporary node pool used to cycle the default node pool for VM resizing. the `var.agents_size` is no longer ForceNew and can be resized by specifying `temporary_name_for_rotation`"
}

variable "agents_availability_zones" {
  type        = list(string)
  default     = ["1", "2"]
  description = "(Optional) A list of Availability Zones across which the Node Pool should be spread. Changing this forces a new resource to be created."
}

variable "agents_type" {
  type        = string
  default     = "VirtualMachineScaleSets"
  description = "(Optional) The type of Node Pool which should be created. Possible values are AvailabilitySet and VirtualMachineScaleSets. Defaults to VirtualMachineScaleSets."
}

variable "agents_size" {
  type        = string
  default     = "Standard_D8_v4"
  description = "The default virtual machine size for the Kubernetes agents. Changing this without specifying `var.temporary_name_for_rotation` forces a new resource to be created."
}

variable "ingress_application_gateway_enabled" {
  type        = bool
  default     = false
  description = "Whether to deploy the Application Gateway ingress controller to this Kubernetes Cluster?"
  nullable    = false
}

variable "ingress_application_gateway_name" {
  type        = string
  default     = "penfieldai-app-gw"
  description = "The name of the Application Gateway to be used or created in the Nodepool Resource Group, which in turn will be integrated with the ingress controller of this Kubernetes Cluster."
}

variable "ingress_application_gateway_subnet_cidr" {
  type        = string
  default     = "10.250.96.0/20"
  description = "The subnet CIDR to be used to create an Application Gateway, which in turn will be integrated with the ingress controller of this Kubernetes Cluster."
}

variable "network_policy" {
  type        = string
  default     = "azure"
  description = " (Optional) Sets up network policy to be used with Azure CNI. Network policy allows us to control the traffic flow between pods. Currently supported values are calico and azure. Changing this forces a new resource to be created."
}

variable "net_profile_dns_service_ip" {
  type        = string
  default     = "10.0.0.10"
  description = "(Optional) IP address within the Kubernetes service address range that will be used by cluster service discovery (kube-dns). Changing this forces a new resource to be created."
}

variable "net_profile_service_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "(Optional) The Network Range used by the Kubernetes service. Changing this forces a new resource to be created."
}
