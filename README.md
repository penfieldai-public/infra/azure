# Terraform for PenfieldAI infrastructure on Azure

This repository contains the code responsible for creating the necessary components in Azure, such as VNets, LBs, AKS clusters, and other required infrastructure elements. These components are essential for the successful execution of the penfieldai applications.

## Repository structure:
```
├─ azure
    ├── README
    │
    ├── infrastructure
    │    ├── aks-cluster.tf      # Azure AKS configuration
    │    ├── backend.tf          # Configure terraform backend to store state file
    │    ├── database.tf         # Azure Flexible PostgreSQL DB configuration
    │    ├── outputs.tf          # Required output of code
    │    ├── providers.tf        # Configure the terraform providers
    |    ├── resource-group.tf   # Configure the resource group
    │    ├── terraform.tf        # configure the terraform and provider versions
    │    ├── variables.tf        # Set up the variables
    │    ├── vnets.tf            # Azure VNet configuration

```

## Prerequisite:

* **Fully Qualified Domain Name:** To ensure proper routing of all traffic to the AKS cluster, a fully qualified domain name (FQDN) is required. This FQDN will be used to point to the load balancer, which will serve as the sole entry point for all incoming traffic directed towards the AKS cluster.

* **VPN:** To access and manage the private resources, including the cluster and load balancer endpoint/DNS, a VPN solution is required. This VPN solution will enable Analysts, Managers, or Infrastructure Administrators to securely connect to the private network and gain access to these resources. Additionally, VPN access needs to be granted to Penfield for this specific cluster and endpoint to facilitate their management and configuration tasks.

* **Provide SSL certificate for Internal load balancer:** In Azure, SSL termination is not performed at the load balancer level. Therefore, an SSL certificate must be available to be added to the internal load balancer for Azure. This SSL certificate will enable secure communication between clients and the internal load balancer, ensuring the confidentiality and integrity of the data transmitted.

* **Grant Access to Azure subscription:** To grant access to the subscription and AKS cluster for the penfield team, create an Active Directory group specifically for them. This group will serve as the means to authorize and manage their permissions within the subscription and AKS cluster.

### 1. Fully Qualified Domain Name:

    Once you have determined the fully qualified domain name (FQDN) that you would like to use for this project, please keep it readily available. An example of an FQDN could be `https://demo.penfield.ai`.

### 2. Generate SSL certificate: 

    After selecting the FQDN as mentioned in the previous step, please proceed with creating an SSL certificate for the chosen FQDN. Kindly follow your internal process for generating the SSL certificate. Once the SSL certificate is ready, please share it with the Penfield team.

### 3. Grant Access to Azure subscription:
    
    To fulfill the requirement, please follow the steps below:

    1. Access your Active Directory management tool.
    2. Create a new group with the name `penfield_aks_cluster_admins`.
    3. Add the Penfield team members to this newly created group.
    4. Navigate to the Azure portal and access the subscription settings.
    5. Grant the `Reader` access role to the `penfield_aks_cluster_admins` group.
    6. Verify that the group members now have the appropriate access to the subscription.

    By completing these steps, the `penfield_aks_cluster_admins` group will be created and the Penfield team members will have `Reader` access to the subscription.

### 4. Create Infrastructure:

`Infrastructure` directory contains all the terraform code to setup all the Infrastructure components that includes VNet, NAT Gateway, Internet Gateway, Application Load Balancer, AKS cluster, AKS Managed Node Group, Auto Scaling group, Security groups, Flexible PostgreSQL cluster that are required for the PenfieldAI application. Please refer to the [Repository structure](#repository-structure) to see the purpose of various files.

In order to create the rest of the infrastructure components, go to [infrastructure](./infrastructure) directory and edit `variables.tf` file with you inputs. Following inputs are available, (We recommend you to keep the default values though if that does not cause any interference, except for the azure_subscription_id value which is specific to your azure subscription):

#### Inputs
| Name  | Description | Type  | Default  |
|-------|-------------|-------|----------|
| `environment` | Name of the environment | `string` | `prod` |
| `project` | Name of the project | `string` | `penfieldai` |
| `terraform` | Managed by Terraform | `string` | `true` |
| `azure_subscription_id` | subscription ID of your Azure instance | `string` | "" |
| `penfieldai_rg_name` | Name of the resource group for the penfield resources | `string` | "penfieldai-resources-rg" |
| `penfieldai_rg_location` | Location of the resource group for the penfield resources | `string` | "canadacentral" |
| `web_subnet1_name` | subnet that the kubernetes instance will reside on | `string` | `web-subnet-1` |
| `public_subnet1_name` | public subnet for azure | `string` | `public-subnet-1` |
| `db_subnet1_name` | subnet that the PostgreSQL instance will reside on | `string` | `db-subnet-1` |
| `use_for_each` | Used for loop to create resources | `bool` | `true` |
| `vnet_name_penfieldai` | name of Vnet | `string` | `penfieldai-vnet` |
| `address_space` | IP range for the Vnet | `string` | `10.250.0.0/16` |
| `public_subnet_prefix` | IP Range for the Public Subnet | `string` | `10.250.0.0/20` |
| `web_subnet_prefix` | IP Range for the web/aks Subnet | `string` | `10.250.32.0/20` |
| `db_subnet_prefix` | IP Range for the DB Subnet | `string` | `10.250.64.0/20` |
| `create_db` | Controls if DB server should be created | `bool` | `false` |
| `database_server_name` | Name of the postgresql cluster | `string` | `penfieldai-postgresql` |
| `database_version` | PostgreSQL version | `string` | `13` |
| `database_username` | admin user for postgresql | `string` | `penfieldaiadm` |
| `db_name1` | database name for postgres | `string` | `penfieldai` |
| `db_name2` | database name for postgres | `string` | `penfieldai-app` |
| `database_server_size` | server size of the postgresql database instance | `string` | `GP_Standard_D2s_v3` |
| `retention` | how many days will database backups be stored for | `number` | `30` |
| `client_id` | The Client ID (appId) for the Service Principal used for the AKS deployment | `string` | "" |
| `client_secret` | The Client Secret (password) for the Service Principal used for the AKS deployment | `string` | "" |
| `kubernetes_version` | Specify which Kubernetes release to use. | `string` | "1.28" |
| `cluster_name` | Name of the EKS cluster | `string` | `penfield-aks-cluster` |
| `network_plugin` | Network plugin to use for networking | `string` | `azure` |
| `os_disk_size_gb` | Size of AKS Instance disk in GB | `number` | `50` |
| `sku_tier` | The SKU Tier that should be used for this Kubernetes Cluster.| `string` | `Standard` |
| `role_based_access_control_enabled` | Enable Role Based Access Control | `bool` | `true` |
| `rbac_aad` | Enable Azure Active Directory integration | `bool` | `true` |
| `rbac_aad_managed` | Azure will create/manage the Service Principal used for AD integration. | `bool` | `true` |
| `rbac_aad_admin_group_object_ids` | Object ID of groups with admin access | `list(string)` | `["penfield_aks_cluster_admins"]` |
| `public_network_access_enabled` | Enable public network access for this Kubernetes Cluster | `bool` | `true` |
| `api_server_authorized_ip_ranges` | The IP ranges to allow for incoming traffic to the server nodes. | `set(string)` | `["163.116.141.0/24","163.116.130.0/24","163.116.142.0/24"]` |
| `private_cluster_enabled` | Make this Kubernetes Cluster private | `bool` | `false` |
| `http_application_routing_enabled` | Enable HTTP Application Routing Addon (forces recreation) | `bool` | `false` |
| `azure_policy_enabled` | Enable Azure Policy Addon | `bool` | `false` |
| `enable_auto_scaling` | Enable node pool autoscaling | `bool` | `true` |
| `enable_host_encryption` | Enable Host Encryption for default node pool. Encryption at host feature must be enabled on the subscription: https://docs.microsoft.com/azure/virtual-machines/linux/disks-enable-host-based-encryption-cli" | `bool` | `true` |
| `agents_min_count` | Minimum number of nodes in a pool | `number` | `2` |
| `agents_max_count` | Maximum number of nodes in a pool | `number` | `5` |
| `agents_count` | The number of Agents that should exist in the Agent Pool | `number` | `null` |
| `agents_max_pods` | The maximum number of pods that can run on each agent. | `number` | `100` |
| `agents_pool_name` | The name of Azure AKS agentpool (nodepool) | `string` | `webnodepool` |
| `temporary_name_for_rotation` | Specifies the name of the temporary node pool | `string` | `webnodepool1` |
| `agents_availability_zones` | A list of Availability Zones across which the Node Pool should be spread | `list(string)` | `["1", "2"]` |
| `agents_type` | The type of Node Pool which should be created | `string` | `VirtualMachineScaleSets` |
| `agents_size` | The default virtual machine size for the Kubernetes agents | `string` | `Standard_D8_v4` |
| `ingress_application_gateway_enabled` | Whether to deploy the Application Gateway ingress controller to this Kubernetes Cluster? | `bool` | `false` |
| `ingress_application_gateway_name` | The name of the Application Gateway to be created | `string` | `penfieldai-app-gw` |
| `ingress_application_gateway_subnet_cidr` | The subnet CIDR to be used to create an Application Gateway | `string` | `10.250.96.0/20` |
| `network_policy` | Sets up network policy to be used with Azure CNI | `string` | `azure` |
| `net_profile_dns_service_ip` | IP address within the Kubernetes service address range that will be used by cluster service discovery | `string` | `10.0.0.10` |
| `net_profile_service_cidr` | The Network Range used by the Kubernetes service | `string` | `10.0.0.0/16` |


**Terraform commands:**
* `terraform init`
* `terraform plan`
* `terraform apply`

## What needs to be share with Penfield?

In the [Step 4](#4-create-infrastructure) The Terraform output will generate certain information that needs to be securely shared with the PenfieldAI team. This information is necessary to grant Penfield team access to the cluster for setting up the Penfield product. In some cases, it may include details about VPN access if you have configured and established VPN access to your private AKS cluster.

* [Optional] In the event that a dedicated database has been created, it is necessary to create a PostgreSQL user and provide access to both databases. The credentials for this user should be shared with the designated party, penfieldai, to enable successful connectivity of the applications to the PostgreSQL database.
